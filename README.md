# Z-Day

An interpretation of of Radomir 'The Sheep' Dopieralski's [Z-Day](http://sheep.art.pl/Z-Day)  
Written in [Go](https://golang.org) and compiled to Javascript by [GopherJS](https://github.com/gopherjs/gopherjs).  
It also compiles to a native terminal application.

[Play online](http://intevation.de/~teichmann/zday)

This is Free Software governed by the terms of the
GNU General Public License Version 3 or later.  
See [LICENSE](LICENSE) for details.

# Build
You need a running Go installation.

## Terminal version

    $ go get -u -v gitlab.com/sascha.l.teichmann/zday

## Manual build

    $ git clone https://gitlab.com/sascha.l.teichmann/zday
    $ go get -u github.com/nsf/termbox-go
    $ cd zday
    $ go build

## JS version

    This needs Go 1.12.x and not later!

    $ git clone https://gitlab.com/sascha.l.teichmann/zday
    $ GO111MODULE=off go get -u github.com/gopherjs/gopherjs
    $ GO111MODULE=off go get -u honnef.co/go/js/dom
    $ cd zday
    $ gopherjs build

To build a minified JS file:

    $ gopherjs build -m

## WebAssembly version (experimental)

    $ git clone https://gitlab.com/sascha.l.teichmann/zday
    $ cd zday
    $ cp "$(go env GOROOT)/misc/wasm/wasm_exec.js" wasm
    $ GOOS=js GOARCH=wasm go build -o wasm/zday.wasm -ldflags '-s -w'
    $ gzip -9 wasm/zday.wasm

# Run

## Terminal version

    $ ./zday

  or

    $ ./zday -256
    $ ./zday -2

  The later two give you correct gray values but only on terminals
  which support 256 colors.

## JS version

  Open `index.html` in your browser.

## WebAssembly version

  Let a web server serve the wasm directory like

  $ cd wasm
  $ python -m SimpleHTTPServer 8000

  Open `http://localhost:8000` in your browser.

# Play

## Goals

- Survive!
- Find an emergency exit on the first floor!

## Controls

Use ...

- ...  **`Cursor`** keys to move around.
- ...  **`v`** to dash (vault) two fields into a direction.
- ...  **`z`** to shoot (zap) at highlighted enemy.
- ...  **`Z`** to aim at a different enemy.
- ...  **`x`** to eXamine crates and corpses.
- ...  **`X`** to use the escalators.
- ...  **`a`** to **`u`** keys to use items from inventory.
- ...  **`A`** to **`U`** keys to discard items from inventory.
- ...  **`Space`** to wait a round.
- ...  **`Esc`** to quit.
- ...  **`?`** to show help.

## Legend

- **`@`** the player
- **`Z`** the zombies
- **`.`** floor
- **`#`** wall
- **`:`** trash and rubble
- **`+`** various body parts
- **`%`** corpses
- **`&`** crates and boxes
- **`$`** dangerous machinery
- **`<`** escalators
- **`X`** emergency exit
