// Copyright 2014, 2015 by Sascha L. Teichmann
// Use of this source code is governed by the GPLv3 or later license
// that can be found in the LICENSE file.

package main

import "math"

type lightPos struct {
	x, y int
}

type light struct {
	maxRays int
	radius  int
	rayMin  [][]int
	rayMax  [][]int
	lighted []lightPos
}

func newLight(maxRays, radius int) *light {
	rayMax := make([][]int, radius)
	rayMin := make([][]int, radius)
	for i := 0; i < radius; i++ {
		rMin := make([]int, radius)
		rMax := make([]int, radius)
		for j := 0; j < radius; j++ {
			rMin[j] = maxRays
		}
		rayMin[i] = rMin
		rayMax[i] = rMax
	}

	fac := math.Pi / float64(maxRays*2)
	for ray := 0; ray < maxRays; ray++ {
		an := fac * float64(ray)
		s, c := math.Sin(an)/4.0, math.Cos(an)/4.0
		d := 0.0
		for {
			x, y := int(math.Floor(s*d)), int(math.Floor(c*d))
			d++
			if x >= radius || y >= radius {
				break
			}
			rayMin[x][y] = min(rayMin[x][y], ray)
			rayMax[x][y] = max(rayMax[x][y], ray)
		}
	}

	return &light{
		maxRays: maxRays,
		radius:  radius,
		rayMin:  rayMin,
		rayMax:  rayMax,
		lighted: make([]lightPos, 0, 256)}
}

func (l *light) lightPos(x, y int) {
	var nl []lightPos
	n := len(l.lighted)
	if n == cap(l.lighted) {
		nl = make([]lightPos, n+1, n+256)
		copy(nl, l.lighted)
	} else {
		nl = l.lighted[0 : n+1]
	}
	nl[n].x, nl[n].y = x, y
	l.lighted = nl
}

func (l *light) reset() {
	l.lighted = l.lighted[0:0]
}

func (l *light) quart(xpos, ypos, dx, dy int, w *worldmap) {

	rays := make([]bool, l.maxRays)

	corner := l.radius

	origx := xpos

	for y := 0; y < l.radius; y++ {
		if 2*y >= l.radius {
			corner--
		}
		for x := 0; x < corner; x++ {
			ft := w.getField(xpos, ypos)
			if ft == nil {
				break
			}
			shadow := 0.0
			rawWeight := float64(l.maxRays) / float64(l.rayMax[x][y]-l.rayMin[x][y])
			for ray, r := l.rayMin[x][y], l.rayMax[x][y]; ray < r; ray++ {
				if rays[ray] {
					shadow += rawWeight
				}
			}
			if (3.0/2.0)*shadow < float64(l.maxRays) {
				ft.set(tileFlagVisible)
				l.lightPos(xpos, ypos)
			}

			if ft.t.Opaque {
				for ray, r := l.rayMin[x][y], l.rayMax[x][y]; ray < r; ray++ {
					rays[ray] = true
				}
			}

			if xpos += dx; xpos < 0 || xpos >= w.width {
				break
			}
		}
		if ypos += dy; ypos < 0 || ypos >= w.height {
			break
		}
		xpos = origx
	}
}

func (l *light) cast(w *worldmap, x, y int) {
	for _, lighted := range l.lighted {
		if ft := w.getField(lighted.x, lighted.y); ft != nil {
			ft.clear(tileFlagVisible)
			ft.set(tileFlagExplored)
		}
	}
	l.reset()
	l.lightPos(x, y)
	l.quart(x, y, 1, 1, w)
	l.quart(x, y, -1, 1, w)
	l.quart(x, y, 1, -1, w)
	l.quart(x, y, -1, -1, w)
}
