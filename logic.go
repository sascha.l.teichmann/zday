// Copyright 2014, 2015 by Sascha L. Teichmann
// Use of this source code is governed by the GPLv3 or later license
// that can be found in the LICENSE file.

package main

import (
	"math"
	"strconv"
	"strings"
)

const (
	lightRays   = 255
	lightRadius = 12
)

const (
	maxInvItems  = 21
	worldMargin  = 9
	worldBorder  = 2
	heroChar     = '@'
	heroColor    = TWhite
	zombieChar   = 'Z'
	zombieCol    = TGreen
	burningChar  = '*'
	burningColor = TYellow
)

const (
	mapWidth  = 64
	mapHeight = 64
)

type (
	itemKind   int
	tileFlag   int
	effectKind int
)

type gameState func(g *Game, e *TEvent) gameState

const (
	// Item kinds
	itemKindWeapon = itemKind(iota)
	itemKindGun
	itemKindFood
	itemKindTrash
)

const (
	tileFlagNone     = tileFlag(0)
	tileFlagExplored = tileFlag(1) << iota
	tileFlagVisible
	tileFlagCreature
)

func (tf tileFlag) isSet(f tileFlag) bool {
	return tf&f != 0
}

func (tf tileFlag) areAllSet(f tileFlag) bool {
	return tf&f == f
}

type effect struct {
	ch  rune
	col int16
}

type tile struct {
	Name      string
	Change    int
	Lethal    int
	Ch        rune
	Color     int16
	Item      bool
	Shadow    bool
	Block     bool
	Opaque    bool
	Highlight bool
}

type flaggedTile struct {
	t   *tile
	f   tileFlag
	ch  rune
	col int16
}

type worldmap struct {
	xOfs   int
	yOfs   int
	width  int
	height int
	fields []flaggedTile
}

type item struct {
	Name       string
	Sound      string
	Kind       itemKind
	CrateProb  int
	CorpseProb int
	Accuracy   int
	Blood      int
	Lethal     int
	Charges    int
	Heal       int
	Food       int
	Burn       bool
	Push       bool
	Noisy      bool
}

type chargedItem struct {
	charges int
	it      *item
}

type inventory struct {
	weapon  int
	gun     int
	content []chargedItem
}

type placeable struct {
	x, y             int
	targetX, targetY int
}

type playerState int

const (
	playerWounded playerState = iota
	playerOk
	playerDone
)

type player struct {
	placeable

	inv   *inventory
	torch *light

	stamina int
	wounds  int
	food    int
	aim     int
	ef      effectKind
}

type zombie struct {
	placeable

	ef      effectKind
	burning bool
	dead    bool
	tripped bool
}

type messages struct {
	last []string
	curr []string
}

// Game connects the game logic with user interaction.
type Game struct {
	state gameState

	level int

	hero    *player
	msgs    *messages
	world   *worldmap
	zombies []*zombie

	terminal Terminal
	msgWin   *TextBuffer
	invWin   *TextBuffer
	statsWin *TextBuffer
	floorWin *TextBuffer
}

// NewGame creates a new game connected to a terminal.
func NewGame(terminal Terminal) *Game {

	hero := newPlayer()
	msgs := newMessages()
	world := newWorldmap(mapWidth, mapHeight)
	zombies := []*zombie{}

	msgWin := NewTextBuffer(terminal.Width()-16, 1, NewAnchor(true, true, 0, 0))
	statsWin := NewTextBuffer(16, 1, NewAnchor(false, true, 16, 0))
	invWin := NewTextBuffer(16, maxInvItems+1, NewAnchor(false, true, 16, 1))
	floorWin := NewTextBuffer(12, 1, NewAnchor(false, false, 12, 2))

	game := &Game{
		state:    splashState,
		level:    0,
		hero:     hero,
		world:    world,
		zombies:  zombies,
		msgs:     msgs,
		terminal: terminal,
		msgWin:   msgWin,
		statsWin: statsWin,
		invWin:   invWin,
		floorWin: floorWin}

	terminal.AddWindow("stats", statsWin)
	terminal.AddWindow("floor", floorWin)
	terminal.AddWindow("msg", msgWin)
	terminal.AddWindow("inv", invWin)
	terminal.AddWindow("map", game)
	terminal.AddWindow("clear", TBlankCell)
	registerTextWindows(terminal)

	terminal.SetEventHandler(game)

	return game
}

func (g *Game) display(win string) {
	t := g.terminal
	t.UpdateWindow("clear")
	t.UpdateWindow(win)
	t.Flush()
}

// Start starts the game engine.
func (g *Game) Start() {
	g.display("splash")
}

func (g *Game) mapWindowSize() (int, int) {
	w, h := g.terminal.Size()
	return max(0, w-16), max(0, h-1)
}

// Resize adjusts the world map to a resized terminal window,
func (g *Game) Resize(screenWidth, screenHeight int) (x, y, width, height int) {
	x, y = 0, 1
	width = max(0, screenWidth-16)
	height = max(0, screenHeight-1)
	g.world.center(g.hero.x, g.hero.y, width, height)
	return
}

// HandleEvent is called from the terminal to handle the events.
func (g *Game) HandleEvent(event *TEvent) {
	//log.Printf("key code: %d '%c'\n", event.Key, event.Key)
	g.state = g.state(g, event)
}

func (g *Game) start() {
	g.msgs.reset()
	g.hero.reset()

	g.newLevel(0)
}

var (
	floorBackground = MakeCell(' ', TLightBlue, TBlack, TBold)
	floorAttr       = MakeAttr(TLightBlue, TBlack, TBold)
)

func (g *Game) newLevel(level int) {

	g.level = level
	g.world.reset()
	g.generateLevel()

	for i := range g.zombies {
		g.zombies[i] = nil
	}
	g.zombies = g.zombies[0:0]

	g.populate()

	hx, hy, _ := g.world.findEmptyField()
	g.hero.setPos(hx, hy)
	f := g.world.getField(hx, hy)
	f.set(tileFlagCreature)
	f.t = &tiles[tileEscalatorUp]
	f.ch = heroChar
	f.col = heroColor
	mw, mh := g.mapWindowSize()
	g.world.center(hx, hy, mw, mh)

	g.hero.torch.reset()
	g.hero.torch.cast(g.world, hx, hy)
	g.hero.retarget(g)

	g.hero.inv.draw(g.invWin)
	g.hero.drawStats(g.statsWin)
	g.msgs.draw(g.msgWin)

	g.floorWin.Clear(floorBackground)
	g.floorWin.Print(
		"Floor "+strconv.Itoa(6-level),
		floorAttr)
}

func (g *Game) generateLevel() {
	if g.level < 5 {
		g.world.generate()
		g.world.decorate()
		g.world.erode()
	} else {
		g.world.final()
	}
}

func (g *Game) populate() {
	for i, n := 0, randRange(50+5*g.level, 90+20*g.level); i < n; i++ {
		if x, y, success := g.world.findEmptyField(); success {
			z := newZombie(x, y)
			field := g.world.getField(x, y)
			field.set(tileFlagCreature)
			field.ch = zombieChar
			field.col = zombieCol
			g.zombies = append(g.zombies, z)
		} else {
			// No more room.
			break
		}
	}
}

func (g *Game) displayGameDynamic() {
	t := g.terminal
	t.UpdateWindow("msg")
	t.UpdateWindow("stats")
	t.UpdateWindow("inv")
	t.UpdateWindow("map")
}

func (g *Game) displayGame() {
	g.displayGameDynamic()
	g.terminal.UpdateWindow("floor")
	g.terminal.Flush()
}

func splashState(g *Game, event *TEvent) gameState {
	if event.Key != TSpc {
		return splashState
	}
	g.display("story")
	return storyState
}

func storyState(g *Game, event *TEvent) gameState {
	if event.Key != TSpc {
		return storyState
	}
	g.start()
	g.terminal.UpdateWindow("clear")
	g.displayGame()
	return defaultState
}

var moveDirection = map[int][]int{
	TKeyCursorUp:    {0, -1},
	TKeyCursorDown:  {0, 1},
	TKeyCursorLeft:  {-1, 0},
	TKeyCursorRight: {1, 0},
	TKey8:           {0, -1},
	TKey2:           {0, 1},
	TKey4:           {-1, 0},
	TKey6:           {1, 0},
	TKey7:           {-1, -1},
	TKey9:           {1, -1},
	TKey1:           {-1, 1},
	TKey3:           {1, 1},
}

func defaultState(g *Game, event *TEvent) gameState {

	if dir, found := moveDirection[event.Key]; found {
		return g.movePlayer(dir[0], dir[1])
	}

	switch {
	case event.Key == TSpc:
		return g.wait()

	case event.Key == TEsc:
		return g.createQuitState()

	case event.Key == TQuest:
		return g.createHelpState()

	case event.Key >= TKeyA && event.Key <= TKeyU:
		if event.Shift {
			return g.trash(event.Key - TKeyA)
		}
		return g.use(event.Key - TKeyA)

	case event.Key == TKeyV:
		return g.queryDirection()

	case event.Key == TKeyX:
		if event.Shift {
			return g.operate()
		}
		return g.examine()

	case event.Key == TKeyZ:
		if event.Shift {
			return g.nextTarget()
		}
		return g.shoot()
	}

	return defaultState
}

func runState(g *Game, event *TEvent) gameState {
	if dir, found := moveDirection[event.Key]; found {
		return g.runPlayer(dir[0], dir[1])
	}
	switch event.Key {
	case TEsc:
		return g.createQuitState()
	case TQuest:
		return g.createHelpState()
	}
	return defaultState
}

func (g *Game) doEffects() (applied bool) {
	w := g.world
	for _, z := range g.zombies {
		if !z.dead && z.ef != effectNone {
			applied = true
			f := w.getField(z.x, z.y)
			f.ch = effects[z.ef].ch
			f.col = effects[z.ef].col
		}
	}
	if h := g.hero; h.ef != effectNone {
		applied = true
		f := w.getField(h.x, h.y)
		f.ch = effects[h.ef].ch
		f.col = effects[h.ef].col
	}
	if applied {
		g.terminal.UpdateWindow("map")
	}
	g.terminal.Flush()
	return
}

func (g *Game) undoEffects() {
	w := g.world
	for _, z := range g.zombies {
		if !z.dead && z.ef != effectNone {
			f := w.getField(z.x, z.y)
			z.ef = effectNone
			if z.burning {
				f.ch = burningChar
				f.col = burningColor
			} else {
				f.ch = zombieChar
				f.col = zombieCol
			}
		}
	}
	if h := g.hero; h.ef != effectNone {
		h.ef = effectNone
		f := w.getField(h.x, h.y)
		f.ch = heroChar
		f.col = heroColor
	}
	g.terminal.UpdateWindow("map")
	g.terminal.Flush()
}

// In "real" Go we would use sync.Once here to
// avoid races, but it seems to be broken in current GopherJS.
// JavasScript isn't racy here so it doesn't matter.
func once(f func()) func() {
	return func() {
		if f != nil {
			g := f
			f = nil
			g()
		}
	}
}

func (g *Game) createEffectState() gameState {
	if g.doEffects() {
		undoEffects := once(func() {
			g.undoEffects()
		})
		g.terminal.SetTimeout(undoEffects, 300)
		return func(g *Game, event *TEvent) gameState {
			undoEffects()
			return defaultState(g, event)
		}
	}
	g.terminal.Flush()
	return defaultState
}

func (g *Game) wait() gameState {
	g.msgs.add(msgYawn)
	return g.executeStep()
}

func (g *Game) movePlayer(dx, dy int) gameState {
	if g.moveHero(dx, dy) == playerDone {
		return againState
	}
	return g.executeStep()
}

func (g *Game) runPlayer(dx, dy int) gameState {
	if g.runHero(dx, dy) == playerDone {
		return againState
	}
	return g.executeStep()
}

func (g *Game) queryDirection() gameState {
	g.msgs.add(msgWhereToRun)
	g.msgs.draw(g.msgWin)
	g.terminal.UpdateWindow("msg")
	g.terminal.Flush()
	return runState
}

func (g *Game) runHero(dx, dy int) playerState {
	if moved := g.moveHero(dx, dy); moved != playerOk {
		return moved
	}
	if g.hero.stamina > 0 {
		switch moved := g.moveHero(dx, dy); moved {
		case playerOk:
			g.hero.stamina = max(0, g.hero.stamina-3)
		case playerDone:
			return playerDone
		}
	} else {
		g.msgs.add(msgPantPant)
	}
	return playerOk
}

func (g *Game) operate() gameState {
	x, y := g.hero.x, g.hero.y
	if f := g.world.getField(x, y); f != nil && f.t == &tiles[tileEscalatorDown] {
		g.newLevel(g.level + 1)
		g.displayGame()
	}
	return defaultState
}

func (g *Game) nextTarget() gameState {
	g.hero.actAim(g)
	g.displayPlayerStats()
	return defaultState
}

func (g *Game) shoot() gameState {
	if g.hero.actShoot(g) {
		return g.executeStep()
	}
	g.displayPlayerStats()
	return defaultState
}

func (g *Game) use(slot int) gameState {
	it := g.hero.inv.content[slot].it
	if it == nil {
		g.msgs.add(msgItsNotThere)
	} else {
		switch it.Kind {
		case itemKindWeapon:
			g.hero.inv.selectWeapon(slot)
		case itemKindGun:
			g.hero.inv.selectGun(slot)
		case itemKindFood:
			g.applyFood(slot)
			return g.executeStep()
		default:
			g.msgs.add(msgUseless)
		}
	}
	g.displayPlayerStats()
	return defaultState
}

func (g *Game) applyFood(slot int) {
	inv := g.hero.inv
	it := inv.content[slot].it
	g.hero.wounds = max(g.hero.wounds-it.Heal, 0)
	g.hero.food = min(g.hero.food+it.Food, 200)
	inv.emitSound(slot, g.msgs)
	if it.Noisy {
		g.noise()
	}
	inv.useUp(slot, g.msgs)
}

func (g *Game) noise() {
	hx, hy := g.hero.x, g.hero.y
	for _, z := range g.zombies {
		if !z.dead && z.distance(hx, hy) < 10 {
			z.target(hx, hy)
		}
	}
}

func (g *Game) findZombie(x, y int) *zombie {
	for _, z := range g.zombies {
		if !z.dead && z.x == x && z.y == y {
			return z
		}
	}
	return nil
}

func (g *Game) removeDeadZombies() {
	for i := len(g.zombies) - 1; i >= 0; i-- {
		if g.zombies[i].dead {
			copy(g.zombies[i:], g.zombies[i+1:])
			g.zombies[len(g.zombies)-1] = nil
			g.zombies = g.zombies[0 : len(g.zombies)-1]
		}
	}
}

func (g *Game) trash(slot int) gameState {
	if !g.hero.inv.delItem(slot) {
		g.msgs.add(msgGoneAlready)
	}
	g.msgs.add(msgUselesCrap)
	return g.executeStep()
}

func (g *Game) examine() gameState {
	field := g.world.getField(g.hero.x, g.hero.y)
	if field == nil || !field.t.Item {
		g.msgs.add(msgNothingUsefulHere)
		g.displayPlayerStats()
		return defaultState
	}

	idx := -1
	if field.t == &tiles[tileCrate] {
		g.msgs.add(msgSearchingCrate)
		idx = searchCrate()
	} else if field.t == &tiles[tileCorpse] {
		g.msgs.add(msgSearchingCorpse)
		idx = searchCorpse()
	}

	if idx != -1 && idx != itemTrash {
		g.msgs.add(msgFoundSomething)
		if !g.hero.inv.addItem(&items[idx]) {
			g.msgs.add(msgNoMoreRoom)
		} else {
			field.t = &tiles[field.t.Change]
		}
	} else {
		g.msgs.add(msgFoundNothing)
		field.t = &tiles[field.t.Change]
	}
	return g.executeStep()
}

func (g *Game) drawPlayerStats() {
	g.msgs.draw(g.msgWin)
	g.hero.drawStats(g.statsWin)
	g.hero.inv.draw(g.invWin)
	g.displayGameDynamic()
}

func (g *Game) displayPlayerStats() {
	g.drawPlayerStats()
	g.terminal.Flush()
}

func (g *Game) executeStep() gameState {

	for _, z := range g.zombies {
		if !z.dead && z.metabolism(g) {
			if z.act(g) == playerDone {
				return againState
			}
		}
	}
	g.removeDeadZombies()

	g.hero.metabolism(g)

	g.hero.torch.cast(g.world, g.hero.x, g.hero.y)
	g.hero.retarget(g)

	g.world.scrollMap(
		g.hero.x, g.hero.y,
		g.terminal.Width()-16, g.terminal.Height()-1)

	g.drawPlayerStats()

	g.msgs.clear()

	return g.createEffectState()
}

func (g *Game) moveHero(dx, dy int) playerState {
	nx, ny := g.hero.x+dx, g.hero.y+dy
	nf := g.world.getField(nx, ny)
	if nf == nil {
		return playerWounded
	}
	if nf.t.Lethal > 0 {
		g.msgs.add(msgCareful)
		return playerOk
	}
	if nf.t == &tiles[tileExit] {
		g.display("win")
		return playerDone
	}
	if nf.t.Block {
		g.msgs.add(msgBonk)
		return playerOk
	}
	if nf.isSet(tileFlagCreature) {
		if z := g.findZombie(nx, ny); z != nil {
			g.msgs.add(msgPardonMe)
			if !z.push(g.world, dx, dy) {
				return playerOk
			}
		}
	}

	of := g.world.getField(g.hero.x, g.hero.y)
	g.hero.setPos(nx, ny)

	of.clear(tileFlagCreature)
	of.ch = ' '
	of.col = TBlack

	nf.set(tileFlagCreature)
	nf.ch = heroChar
	nf.col = heroColor

	return playerOk
}

func exitState(g *Game, event *TEvent) gameState {
	// Do nothing ... we are done.
	return exitState
}

func (g *Game) quit() gameState {
	if g.terminal.Quit() {
		return exitState
	}
	g.display("splash")
	return splashState
}

func againState(g *Game, event *TEvent) gameState {
	if event.Key == TKeyY {
		g.start()
		g.terminal.UpdateWindow("clear")
		g.displayGame()
		return defaultState
	}
	return g.quit()
}

func (g *Game) createQuitState() gameState {
	g.display("quit")
	resumeState := g.state
	return func(g *Game, event *TEvent) gameState {
		if event.Key == TKeyY {
			return g.quit()
		}
		g.displayGame()
		return resumeState
	}
}

func (g *Game) createHelpState() gameState {
	helpWin := helpWindows()
	g.display(helpWin())
	resumeState := g.state
	var helpState func(*Game, *TEvent) gameState
	helpState = func(g *Game, event *TEvent) gameState {
		if event.Key == TSpc {
			if help := helpWin(); help != "" {
				g.display(help)
				return helpState
			}
		}
		g.displayGame()
		return resumeState
	}
	return helpState
}

func searchItem(f func(*item) int) func() int {
	max, m := 0, 0
	for i := range items {
		if p := f(&items[i]); p > 0 {
			max += p
			m = i
		}
	}
	return func() int {
		for i, s, p := 0, 0, rnd.Intn(max); i <= m; i++ {
			if s += f(&items[i]); s > p {
				return i
			}
		}
		return -1
	}
}

var (
	searchCrate  = searchItem(func(it *item) int { return it.CrateProb })
	searchCorpse = searchItem(func(it *item) int { return it.CorpseProb })
)

func newInventory() *inventory {
	return &inventory{
		weapon:  -1,
		gun:     -1,
		content: make([]chargedItem, maxInvItems)}

}

func (inv *inventory) reset() {
	inv.weapon = -1
	inv.gun = -1

	for i := range inv.content {
		inv.content[i].it = nil
		inv.content[i].charges = 0
	}
}

func (inv *inventory) addItem(it *item) bool {
	for i := range inv.content {
		if inv.content[i].it == nil {
			inv.content[i].it = it
			inv.content[i].charges = it.Charges
			return true
		}
	}
	return false
}

func (inv *inventory) selectEquipment() {
	for m, i, n := 0, 0, len(inv.content); i < n && m != 3; i++ {
		it := inv.content[i].it
		if it == nil {
			continue
		}
		// No weapon, yet.
		if m&1 == 0 && it.Kind == itemKindWeapon {
			inv.weapon = i
			m |= 1
		}
		// No gun, yet.
		if m&2 == 0 && it.Kind == itemKindGun {
			inv.gun = i
			m |= 2
		}
	}
}

func (inv *inventory) selectWeapon(slot int) {
	if inv.weapon == slot {
		inv.weapon = -1
	} else {
		inv.weapon = slot
	}
}

func (inv *inventory) selectGun(slot int) {
	if inv.gun == slot {
		inv.gun = -1
	} else {
		inv.gun = slot
	}
}

func (inv *inventory) isSelected(slot int) bool {
	return slot == inv.weapon || slot == inv.gun
}

func (inv *inventory) delItem(slot int) bool {
	if inv.content[slot].it == nil {
		return false
	}
	switch {
	case slot == inv.weapon:
		inv.weapon = -1
	case slot == inv.gun:
		inv.gun = -1
	}
	inv.content[slot].charges = 0
	inv.content[slot].it = nil
	return true
}

func (inv *inventory) emitSound(slot int, msgs *messages) {
	if s := inv.content[slot].it.Sound; s != "" {
		msgs.add(s)
	}
}

func (inv *inventory) useUp(slot int, msgs *messages) bool {
	if s := &inv.content[slot]; s.charges > 0 {
		if s.charges--; s.charges <= 0 {
			inv.delItem(slot)
			msgs.add(msgEmpty)
			return true
		}
	} else if s.it.Kind != itemKindWeapon {
		inv.delItem(slot)
		return true
	}
	return false
}

var (
	invBackgound   = MakeCell(' ', TGreen, TBlack, TNormal)
	invSelected    = MakeAttr(TLightGreen, TBlack, TBold)
	invNotSelected = MakeAttr(TGreen, TBlack, TNormal)
)

func (inv *inventory) draw(tb *TextBuffer) {
	tb.Clear(invBackgound)
	for i := range inv.content {
		var name string
		var attr int16
		if inv.content[i].it == nil {
			name = "---"
		} else {
			name = inv.content[i].it.Name
		}
		if inv.isSelected(i) {
			attr = invSelected
		} else {
			attr = invNotSelected
		}

		s := string('a'+i) + " " + name
		if charges := inv.content[i].charges; charges > 0 {
			s += " " + strconv.Itoa(charges)
		}
		tb.Println(s, attr)
	}
}

func (p *placeable) untarget() {
	p.targetX, p.targetY = -1, -1
}

func (p *placeable) hasTarget() bool {
	return p.targetX != -1
}

func (p *placeable) onTarget() bool {
	return p.x == p.targetX && p.y == p.targetY
}

func (p *placeable) target(x, y int) {
	p.targetX, p.targetY = x, y
}

func (p *placeable) setPos(x, y int) {
	p.x, p.y = x, y
}

func (p *placeable) getPos() (int, int) {
	return p.x, p.y
}

func (p *placeable) setTarget(x, y int) {
	p.targetX, p.targetY = x, y
}

func (p *placeable) getTarget() (int, int) {
	return p.targetX, p.targetY
}

func (p *placeable) isTargetedAt(x, y int) bool {
	return p.targetX == x && p.targetY == y
}

func (p *placeable) distance(x, y int) int {
	return distance(p.x, p.y, x, y)
}

func (p *placeable) atTarget() bool {
	return p.x == p.targetX && p.y == p.targetY
}

func newZombie(x, y int) *zombie {
	z := &zombie{ef: effectNone}
	z.setPos(x, y)
	z.untarget()
	return z
}

func (z *zombie) burn() {
	z.burning = true
}

func (z *zombie) trip() {
	if !z.burning {
		z.tripped = true
	}
}

func (z *zombie) untrip() {
	if !z.burning {
		z.tripped = false
	}
}

func immutable(t *tile) bool {
	return t == &tiles[tileCrate] ||
		t == &tiles[tileCorpse] ||
		t == &tiles[tileWall] ||
		t == &tiles[tileOpenDoor] ||
		t == &tiles[tileEscalatorDown]
}

func (z *zombie) kill(g *Game) {
	g.msgs.add(msgSplat)
	z.dead = true
	f := g.world.getField(z.x, z.y)
	f.clear(tileFlagCreature)
	f.ch = ' '
	f.col = TBlack
	if !immutable(f.t) {
		if f.t.Lethal > 0 {
			f.t = &tiles[tileBloodyMachinery]
		} else {
			if rnd.Intn(100) > 20 {
				f.t = &tiles[tileCorpse]
			} else {
				f.t = &tiles[tilePileOfFlesh]
			}
		}
	}
}

func (z *zombie) wound(g *Game, leathality int) bool {
	if leathality > 0 {
		z.ef = effectHit
		if rnd.Intn(100) < leathality {
			z.kill(g)
			return false
		}
		if leathality > 50 {
			z.trip()
		}
	}
	return true
}

func (z *zombie) metabolism(g *Game) bool {
	if f := g.world.getField(z.x, z.y); f != nil && f.t.Lethal > 0 {
		g.world.bleed(z.x, z.y, 80)
		z.trip()
		if !z.wound(g, 20) {
			return false
		}
	}
	if z.burning {
		z.ef = effectBurn
		if rnd.Intn(100) > 90 {
			z.kill(g)
			return false
		}
	}
	return true
}

func (z *zombie) push(w *worldmap, dx, dy int) bool {
	if dx == 0 && dy == 0 {
		return false
	}

	nx, ny := z.x+dx, z.y+dy

	nf := w.getField(nx, ny)
	if nf == nil {
		return false
	}
	if nf.t.Lethal > 0 {
		z.trip()
	} else if nf.t.Block {
		return false
	}
	if nf.isSet(tileFlagCreature) {
		return false
	}

	of := w.getField(z.x, z.y)
	of.clear(tileFlagCreature)
	of.ch = ' '
	of.col = TBlack

	nf.set(tileFlagCreature)
	if z.burning {
		nf.ch = burningChar
		nf.col = burningColor
	} else {
		nf.ch = zombieChar
		nf.col = zombieCol
	}

	z.setPos(nx, ny)

	return true
}

func (z *zombie) pushback(g *Game) {
	hx, hy := g.hero.getPos()
	dx, dy := towards(hx, hy, z.x, z.y)
	if z.push(g.world, dx, dy) {
		z.trip()
	}
}

func (z *zombie) actGo(g *Game, dx, dy int) bool {
	if dx == 0 && dy == 0 {
		return false
	}
	nx, ny := z.x+dx, z.y+dy
	nf := g.world.getField(nx, ny)
	if nf == nil || nf.t.Block || nf.isSet(tileFlagCreature) {
		return false
	}

	of := g.world.getField(z.x, z.y)
	of.clear(tileFlagCreature)
	of.ch = ' '
	of.col = TBlack

	nf.set(tileFlagCreature)
	if z.burning {
		nf.ch = burningChar
		nf.col = burningColor
	} else {
		nf.ch = zombieChar
		nf.col = zombieCol
	}

	z.setPos(nx, ny)

	return true
}

func (z *zombie) actBite(g *Game) playerState {
	first := randBool()

	if first && !g.hero.defend(g, z) {
		return playerWounded
	}

	pState := playerOk

	if !z.dead && z.distance(g.hero.x, g.hero.y) <= 1 && rnd.Intn(100) > 10 {
		g.msgs.add(msgChomp)
		if pState = g.hero.hitBite(g); pState == playerDone {
			return playerDone
		}
	}

	if !first && !z.dead {
		g.hero.defend(g, z)
	}

	return pState
}

func (z *zombie) enflameNeighbors(g *Game) {
	for nx := z.x - 1; nx <= z.x+1; nx++ {
		for ny := z.y - 1; ny <= z.y+1; ny++ {
			if nx == z.x && ny == z.y {
				continue
			}
			if oz := g.findZombie(nx, ny); oz != nil {
				oz.burn()
				f := g.world.getField(nx, ny)
				f.ch = burningChar
				f.col = burningColor
			}
		}
	}
	if z.distance(g.hero.x, g.hero.y) <= 1 {
		g.hero.burn(g)
	}
}

func (z *zombie) act(g *Game) playerState {
	if z.burning {
		z.enflameNeighbors(g)
		dx, dy := randRange(-1, 1), randRange(-1, 1)
		z.actGo(g, dx, dy)
		return playerOk
	}
	if z.tripped {
		z.untrip()
		return playerOk
	}
	if z.atTarget() {
		z.untarget()
	}
	if g.world.getField(z.x, z.y).isSet(tileFlagVisible) {
		z.setTarget(g.hero.x, g.hero.y)
	}
	if !z.hasTarget() {
		return playerOk
	}

	hx, hy := z.getTarget()
	pState := playerOk
	if z.distance(hx, hy) <= 1 {
		if pState = z.actBite(g); pState == playerDone {
			return playerDone
		}
	} else {
		dx, dy := towards(z.x, z.y, hx, hy)
		if !z.actGo(g, dx, dy) {
			if randBool() {
				z.actGo(g, dx, 0)
			} else {
				z.actGo(g, 0, dy)
			}
		}
	}
	return pState
}

func newPlayer() *player {
	p := &player{
		inv:   newInventory(),
		torch: newLight(lightRays, lightRadius)}
	p.reset()
	return p
}

func (p *player) reset() {
	p.stamina = 100
	p.wounds = 0
	p.food = 0
	p.aim = -1
	p.ef = effectNone
	p.setPos(0, 0)
	p.untarget()

	p.inv.reset()
	p.inv.addItem(&items[itemUmbrella])
	p.inv.addItem(&items[itemNailGun])
	p.inv.addItem(&items[itemWoundSpray])
	p.inv.selectEquipment()

	p.torch.reset()
}

func (p *player) retarget(g *Game) {
	if p.hasTarget() {
		if p.onTarget() ||
			!g.world.getField(p.targetX, p.targetY).areAllSet(tileFlagVisible|tileFlagCreature) {
			p.untarget()
		}
	}
	if !p.hasTarget() {
		dmin := math.MaxInt32
		for _, z := range g.zombies {
			if !z.dead && !z.burning &&
				g.world.getField(z.x, z.y).isSet(tileFlagVisible) {
				if d := p.distance(z.x, z.y); d < dmin {
					dmin = d
					p.target(z.x, z.y)
				}
			}
		}
	}
}

func (p *player) metabolism(g *Game) {
	if p.food > 0 {
		amount := min(randRange(3, 6), p.food)
		p.food -= amount
		p.stamina += amount
	}
	p.stamina -= p.wounds
	switch {
	case p.stamina < 0:
		p.stamina = 0
	case p.stamina > 100:
		p.stamina = 100
	}
}

func (p *player) defend(g *Game, z *zombie) bool {
	slot := p.inv.weapon
	if p.stamina <= 0 || slot == -1 {
		return true
	}

	weapon := p.inv.content[slot].it

	zAlive := true

	if rnd.Intn(100) <= weapon.Accuracy {
		p.inv.emitSound(slot, g.msgs)
		if weapon.Noisy {
			g.noise()
		}
		if weapon.Blood > 0 {
			g.world.bleed(z.x, z.y, weapon.Blood)
		}
		if weapon.Burn {
			z.burn()
		}
		if weapon.Push {
			z.pushback(g)
		}

		zAlive = z.wound(g, weapon.Lethal)
	} else {
		z.ef = effectMiss
	}
	if !p.inv.useUp(slot, g.msgs) {
		if rnd.Intn(100) < 15 {
			g.msgs.add(msgBroken)
			p.inv.delItem(slot)
		}
	}
	return zAlive
}

func (p *player) hitBite(g *Game) playerState {
	if rnd.Intn(100) > 10 {
		g.msgs.add(msgOuch)
		g.world.bleed(p.x, p.y, 20)
		return p.wound(g)
	}
	return playerOk
}

func (p *player) burn(g *Game) {
	g.msgs.add(msgBurns)
	if p.stamina -= randRange(10, 40); p.stamina < 0 {
		p.stamina = 0
	}
}

func (p *player) actShoot(g *Game) bool {

	if !p.hasTarget() {
		g.msgs.add(msgNobodyHere)
		return false
	}

	slot := p.inv.gun
	if slot == -1 {
		g.msgs.add(msgClick)
		return true
	}

	z := g.findZombie(p.targetX, p.targetY)
	if z == nil {
		g.msgs.add(msgSneakyBastard)
		return false
	}

	p.inv.emitSound(slot, g.msgs)

	gun := p.inv.content[slot].it

	if gun.Noisy {
		g.noise()
	}

	if rnd.Intn(100) <= gun.Accuracy {
		if gun.Blood > 0 {
			g.world.bleed(z.x, z.y, gun.Blood)
		}
		if gun.Burn {
			z.burn()
		}
		if gun.Push {
			z.pushback(g)
		}
		z.wound(g, gun.Lethal)
	} else {
		g.msgs.add(msgSpak)
		z.ef = effectMiss
	}
	p.inv.useUp(slot, g.msgs)

	return true
}

func (p *player) wound(g *Game) playerState {
	p.ef = effectHit
	if p.stamina <= 0 {
		g.display("lose")
		return playerDone
	}
	if p.wounds < 5 {
		p.wounds++
	}
	if p.stamina -= randRange(1, 10); p.stamina < 0 {
		p.stamina = 0
	}
	return playerWounded
}

func (p *player) actAim(g *Game) {
	g.msgs.add(msgChangedYourMind)

	n := len(g.zombies)

	if n == 0 {
		p.aim = -1
	} else if p.aim == -1 {
		p.aim = 0
	}

	if p.aim == -1 {
		g.msgs.add(msgNoMoreTargets)
		p.untarget()
		return
	}

	start := p.aim
	for {
		if p.aim++; p.aim >= n {
			p.aim = 0
		}
		z := g.zombies[p.aim]
		if !z.dead && g.world.getField(z.x, z.y).isSet(tileFlagVisible) {
			g.msgs.add(msgHere)
			p.setTarget(z.x, z.y)
			break
		}
		if p.aim == start {
			g.msgs.add(msgNoMoreTargets)
			break
		}
	}
}

var (
	statsBackground = MakeCell(' ', TRed, TBlack, TBold)
	statsAttr       = MakeAttr(TRed, TBlack, TBold)
)

func (p *player) drawStats(tb *TextBuffer) {
	tb.Clear(statsBackground)
	wounds := strings.Repeat("*", p.wounds)
	st := strconv.Itoa(p.stamina)
	if l := len(st); l < 3 {
		st = strings.Repeat(" ", 3-l) + st
	}
	tb.Print("S:"+st+" W:"+wounds, statsAttr)
}

func newMessages() *messages {
	return &messages{
		last: []string{},
		curr: []string{}}
}

func (msgs *messages) reset() {
	msgs.last = msgs.last[0:0]
	msgs.curr = msgs.curr[0:0]
}

var (
	msgBackground = MakeCell(' ', TGreen, TBlack, TNormal)
	msgLastAttr   = MakeAttr(TGreen, TBlack, TNormal)
	msgCurrAttr   = MakeAttr(TLightGreen, TBlack, TBold)
)

func (msgs *messages) draw(tb *TextBuffer) {
	tb.Clear(msgBackground)
	first := true
	for _, m := range msgs.last {
		if first {
			first = false
		} else {
			tb.Print(" ", msgLastAttr)
		}
		tb.Print(m, msgLastAttr)
	}
	for _, m := range msgs.curr {
		if first {
			first = false
		} else {
			tb.Print(" ", msgCurrAttr)
		}
		tb.Print(m, msgCurrAttr)
	}
}

func (msgs *messages) clear() {
	msgs.last, msgs.curr = msgs.curr, msgs.last[0:0]
}

func (msgs *messages) add(m string) {
	msgs.curr = append(msgs.curr, m)
}

var emptyFlaggedTile = flaggedTile{
	t:   &tiles[tileFloor],
	f:   tileFlagNone,
	ch:  ' ',
	col: TBlack}

func newWorldmap(width, height int) *worldmap {

	w := &worldmap{
		fields: make([]flaggedTile, width*height),
		width:  width,
		height: height,
		xOfs:   0,
		yOfs:   0}

	w.fill(&emptyFlaggedTile)

	return w
}

func (w *worldmap) reset() {
	w.xOfs = 0
	w.yOfs = 0
	w.fill(&emptyFlaggedTile)
}

func (w *worldmap) fill(ft *flaggedTile) {
	for i := range w.fields {
		w.fields[i] = *ft
	}
}

func (w *worldmap) bleed(x, y, amount int) {
	for dy := -1; dy <= 1; dy++ {
		for dx := -1; dx <= 1; dx++ {
			field := w.getField(x+dx, y+dy)
			if field == nil {
				continue
			}
			if field.t == &tiles[tileFloor] {
				if rnd.Intn(100) > 110-amount {
					field.t = &tiles[tileBloodPool]
				} else if rnd.Intn(100) > 80-amount {
					field.t = &tiles[tileBloodStain]
				}
			} else if field.t == &tiles[tileWall] {
				if rnd.Intn(100) > 130-amount {
					field.t = &tiles[tileBloodyWall]
				}
			}
		}
	}
}

func (w *worldmap) inside(x, y int) bool {
	return x >= 0 && x < w.width && y >= 0 && y < w.height
}

func (w *worldmap) getField(x, y int) *flaggedTile {
	if w.inside(x, y) {
		return &w.fields[y*w.width+x]
	}
	return nil
}

func (w *worldmap) erode() {
	for i, n := 0, randRange(350, 700); i < n; i++ {
		x := randRange(1, w.width-2)
		y := randRange(1, w.height-2)
		w.change(x, y)
	}
}

func (w *worldmap) change(x, y int) {
	if field := w.getField(x, y); field != nil {
		field.t = &tiles[field.t.Change]
	}
}

func (w *worldmap) findEmptyField() (x, y int, success bool) {

	for tries := 3; tries >= 0; tries-- {
		x = randRange(1, w.width-2)
		y = randRange(1, w.height-2)
		if f := &w.fields[y*w.width+x]; !f.t.Block && !f.isSet(tileFlagCreature) {
			success = true
			return
		}
	}
	sx, sy := x, y
	for {
		x++
		if x >= w.width {
			x = 0
			if y++; y >= w.height {
				y = 0
			}
		}
		if x == sx && y == sy {
			break
		}
		if f := &w.fields[y*w.width+x]; !f.t.Block && !f.isSet(tileFlagCreature) {
			success = true
			return
		}
	}
	return
}

func (w *worldmap) center(x, y, width, height int) {
	w.xOfs = x - width/2
	w.yOfs = y - height/2
}

func (w *worldmap) scrollMap(x, y, width, height int) {
	// TODO: Simplify this!
	if x < w.xOfs+worldMargin {
		w.xOfs = max(-worldBorder, w.xOfs-worldMargin)
		if x >= w.xOfs+width-worldMargin {
			w.xOfs = min(w.width-width+worldBorder, w.xOfs+worldMargin)
		}
	} else if x >= w.xOfs+width-worldMargin {
		w.xOfs = min(w.width-width+worldBorder, w.xOfs+worldMargin)
		if x < w.xOfs+worldMargin {
			w.xOfs = max(-worldBorder, w.xOfs-worldMargin)
		}
	}
	if y < w.yOfs+worldMargin {
		w.yOfs = max(-worldBorder, w.yOfs-worldMargin)
		if y >= w.yOfs+height-worldMargin {
			w.yOfs = min(w.height-height+worldBorder, w.yOfs+worldMargin)
		}
	} else if y >= w.yOfs+height-worldMargin {
		w.yOfs = min(w.height-height+worldBorder, w.yOfs+worldMargin)
		if y < w.yOfs+worldMargin {
			w.yOfs = max(-worldBorder, w.yOfs-worldMargin)
		}
	}
}

// CellAt serves the world map to the terminal.
func (g *Game) CellAt(x, y int) Cell {

	w := g.world

	x += w.xOfs
	y += w.yOfs

	if !w.inside(x, y) {
		return TBlankCell
	}

	ft := &w.fields[y*w.width+x]

	flags := ft.f

	if flags.areAllSet(tileFlagVisible | tileFlagCreature) {
		ch, col := ft.ch, ft.col
		bg, weight := TBlack, TNormal
		if g.hero.isTargetedAt(x, y) {
			if col == TRed {
				bg = TLightBlue
			} else {
				bg = TRed
			}
			weight = TBold
		}
		if col == bg {
			col = TLightBlue
		}
		return MakeCell(ch, col, bg, weight)
	}
	if flags.isSet(tileFlagVisible) {
		col := ft.t.Color
		if ft.t.Shadow {
			if s := w.getField(x-1, y-1); s != nil && s.t.Opaque {
				col = TGray
			}
		}
		return MakeCell(ft.t.Ch, col, TBlack, TNormal)
	}
	if flags.isSet(tileFlagExplored) {
		weight := TNormal
		if ft.t.Highlight {
			weight = TBold
		} else if ft.t.Shadow {
			if s := w.getField(x-1, y-1); s != nil && !s.t.Opaque {
				weight = TBold
			}
		}
		return MakeCell(ft.t.Ch, TMagenta, TBlack, weight)
	}
	return TBlankCell
}

func (ft *flaggedTile) isSet(m tileFlag) bool {
	return ft.f&m != 0
}

func (ft *flaggedTile) areAllSet(m tileFlag) bool {
	return ft.f&m == m
}

func (ft *flaggedTile) set(m tileFlag) {
	ft.f |= m
}

func (ft *flaggedTile) clear(m tileFlag) {
	ft.f &= ^m
}
