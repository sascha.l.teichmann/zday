// Copyright 2014, 2015 by Sascha L. Teichmann
// Use of this source code is governed by the GPLv3 or later license
// that can be found in the LICENSE file.

package main

import (
	"math/rand"
	"time"
)

// Our common random source.
var rnd = rand.New(rand.NewSource(time.Now().Unix()))

func randRange(a, b int) int {
	return a + rnd.Intn(b-a+1)
}

func randBool() bool {
	return rnd.Int()&2 == 2
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func abs(a int) int {
	if a < 0 {
		return -a
	}
	return a
}

func distance(x1, y1, x2, y2 int) int {
	return (max(abs(x2-x1), abs(y2-y1)) + abs(x2-x1) + abs(y2-y1)) / 2
}

func towards(ax, ay, bx, by int) (dx, dy int) {
	if ax < bx {
		dx = +1
	} else if ax > bx {
		dx = -1
	}
	if ay < by {
		dy = +1
	} else if ay > by {
		dy = -1
	}
	return
}
