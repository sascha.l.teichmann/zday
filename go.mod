module gitlab.com/sascha.l.teichmann/zday

go 1.15

require (
	github.com/mattn/go-runewidth v0.0.9 // indirect
	github.com/nsf/termbox-go v0.0.0-20201107200903-9b52a5faed9e
	honnef.co/go/js/dom/v2 v2.0.0-20200509013220-d4405f7ab4d8
)
