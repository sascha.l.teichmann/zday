// Copyright 2014, 2015 by Sascha L. Teichmann
// Use of this source code is governed by the GPLv3 or later license
// that can be found in the LICENSE file.

package main

// Anchor is used to pin a terminal window realtive to a given coordinate.
type Anchor struct {
	left bool
	top  bool
	x    int
	y    int
}

// TextBuffer is a mutable text window.
type TextBuffer struct {
	anchor *Anchor
	width  int
	height int
	x      int
	y      int
	cells  []Cell
}

// TextWindow is an unmutable text window. Useful for splash and info windows.
type TextWindow struct {
	text  []string
	width int
	attr  int16
}

// NewAnchor creates a new Anchor at x/y. top/left are the layout directions.
func NewAnchor(left, top bool, x, y int) *Anchor {
	return &Anchor{left: left, top: top, x: x, y: y}
}

// Position returns the anchor position for a given screen size.
func (a *Anchor) Position(width, height int) (x, y int) {
	if a.left {
		x = a.x
	} else {
		x = max(0, width-a.x)
	}
	if a.top {
		y = a.y
	} else {
		y = max(0, height-a.y)
	}
	return
}

// NewTextBuffer creates a new mutable textbuffer with given size and anchor.
func NewTextBuffer(width, height int, anchor *Anchor) (tb *TextBuffer) {
	cells := make([]Cell, width*height)
	tb = &TextBuffer{
		anchor: anchor,
		width:  width,
		height: height,
		cells:  cells}
	tb.Clear(TBlankCell)
	return
}

// Width reports the width of the TextBuffer.
func (tb *TextBuffer) Width() int {
	return tb.width
}

// Height reports the height of the TextBuffer.
func (tb *TextBuffer) Height() int {
	return tb.height
}

// Clear fills the TextBuffer with a given cell.
func (tb *TextBuffer) Clear(cell Cell) {
	tb.x, tb.y = 0, 0
	for i := range tb.cells {
		tb.cells[i] = cell
	}
}

func (tb *TextBuffer) scrollUp() {
	width := tb.width
	for p, end := 0, len(tb.cells)-width; p < end; p += width {
		copy(tb.cells[p:p+width], tb.cells[p+width:p+2*width])
	}

	for p, end := len(tb.cells)-width, len(tb.cells); p < end; p++ {
		tb.cells[p] = TBlankCell
	}
}

// Print prints a text with a given attribute at the current cursor position.
func (tb *TextBuffer) Print(s string, attr int16) {
	x, y, width := tb.x, tb.y, tb.width
	cells := tb.cells
	pos := width*y + x
	for _, c := range s {
		cells[pos].Ch = c
		cells[pos].Attr = attr
		pos++
		if x++; x == width {
			x = 0
			if y++; y == tb.height {
				tb.scrollUp()
				y--
				pos -= width
			}
		}
	}
	tb.x, tb.y = x, y
}

// Println prints a text with a given attribute at the current cursor position.
// Afterwards the cursor is position at the beginning of the next line.
func (tb *TextBuffer) Println(s string, attr int16) {
	tb.Print(s, attr)
	tb.x = 0
	tb.y++
	if tb.y == tb.height {
		tb.y--
		tb.scrollUp()
	}
}

// CellAt serves the content of the TextBuffer to the terminal.
func (tb *TextBuffer) CellAt(x, y int) Cell {
	return tb.cells[y*tb.width+x]
}

// Resize reports the new size and position of this TextBuffer to the terminal
// if the terminal is resized.
func (tb *TextBuffer) Resize(screenWidth, screenHeight int) (x, y, width, height int) {
	x, y = tb.anchor.Position(screenWidth, screenHeight)
	width = min(tb.width, screenWidth)
	height = min(tb.height, screenHeight)
	return
}

// NewTextWindow creates a new immutable text window with given text and attribute.
func NewTextWindow(text []string, attr int16) *TextWindow {
	return &TextWindow{text: text, width: maxLen(text), attr: attr}
}

// CellAt serves the content of the TextWindow to the terminal.
func (tw *TextWindow) CellAt(x, y int) Cell {
	if y >= len(tw.text) {
		return TBlankCell
	}
	if r := tw.text[y]; x < len(r) {
		return Cell{Ch: rune(r[x]), Attr: tw.attr}
	}
	return TBlankCell
}

// Resize reports the new size and position of this TextWindow to the terminal
// if the terminal is resized.
func (tw *TextWindow) Resize(screenWidth, screenHeight int) (x, y, width, height int) {
	x = max(0, (screenWidth-tw.width)/2)
	y = max(0, (screenHeight-len(tw.text))/2)
	width = min(screenWidth, tw.width)
	height = min(screenHeight, len(tw.text))
	return
}

func maxLen(text []string) (max int) {
	for _, s := range text {
		if l := len(s); l > max {
			max = l
		}
	}
	return
}
