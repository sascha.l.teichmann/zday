// Copyright 2014, 2015 by Sascha L. Teichmann
// Use of this source code is governed by the GPLv3 or later license
// that can be found in the LICENSE file.

// +build js wasm

package main

import "honnef.co/go/js/dom/v2"

var rgb = [...]string{
	TBlack:        "#000000",
	TBlue:         "#0000aa",
	TGreen:        "#00aa00",
	TCyan:         "#00aaaa",
	TRed:          "#aa0000",
	TMagenta:      "#aa00aa",
	TBrown:        "#aa5500",
	TLightGray:    "#aaaaaa",
	TGray:         "#555555",
	TLightBlue:    "#5555ff",
	TLightGreen:   "#55ff55",
	TLightCyan:    "#55ffff",
	TLightRed:     "#ff5555",
	TLightMagenta: "#ff55ff",
	TYellow:       "#ffff55",
	TWhite:        "#ffffff",
}

type window struct {
	name          string
	x, y          int
	width, height int
	visible       bool
	window        TWindow
}

type styledText interface {
	Style() *dom.CSSStyleDeclaration
	SetTextContent(string)
}

func setElementFromCell(element styledText, cell Cell) {
	style := element.Style()
	style.SetProperty("color", rgb[cell.Attr&0xf], "")
	style.SetProperty("background-color", rgb[cell.Attr>>4&0xf], "")
	mode := cell.Attr >> 8 & 3
	if (mode & TBold) == TBold {
		style.SetProperty("font-weight", "bold", "")
	} else {
		style.SetProperty("font-weight", "normal", "")
	}
	c := cell.Ch
	if c == ' ' {
		c = '\u00a0'
	}
	element.SetTextContent(string(c))
}

func setElementFromDiff(element styledText, nCell Cell, oCell *Cell) {
	if nCell.Attr != oCell.Attr {
		style := element.Style()
		if nMode := nCell.Attr >> 8 & 3; nMode != oCell.Attr>>8&3 {
			if (nMode & TBold) == TBold {
				style.SetProperty("font-weight", "bold", "")
			} else {
				style.SetProperty("font-weight", "normal", "")
			}
		}
		if nFg := nCell.Attr & 0xf; nFg != oCell.Attr&0xf {
			style.SetProperty("color", rgb[nFg], "")
		}
		if nBg := nCell.Attr >> 4 & 0xf; nBg != oCell.Attr>>4&0xf {
			style.SetProperty("background-color", rgb[nBg], "")
		}
		oCell.Attr = nCell.Attr
	}
	if c := nCell.Ch; c != oCell.Ch {
		oCell.Ch = c
		if c == ' ' {
			c = '\u00a0'
		}
		element.SetTextContent(string(c))
	}
}

// JSTerminal is a HTML DOM terminal implemenation.
type JSTerminal struct {
	width   int
	cells   []Cell
	tds     []styledText
	windows []*window
}

func removeLoading(div *dom.HTMLDivElement) {
	for _, c := range div.ChildNodes() {
		if e, ok := c.(dom.Element); ok && e.ID() == "loading" {
			div.RemoveChild(c)
			return
		}
	}
}

// NewJSTerminal creates a new terminal of given size inside a <div> with a given id.
func NewJSTerminal(terminalID string, width, height int) *JSTerminal {

	win := dom.GetWindow()
	document := win.Document()
	terminal := document.GetElementByID(terminalID).(*dom.HTMLDivElement)

	// Little dirty hack to remove possible nested "loading..." notifications.
	removeLoading(terminal)

	table := document.CreateElement("table")

	cells := make([]Cell, width*height)
	tds := make([]styledText, width*height)

	cell := MakeCell(' ', TWhite, TBlack, TNormal)

	for pos, i := 0, 0; i < height; i++ {
		row := document.CreateElement("tr")
		for j := 0; j < width; j++ {
			element := document.CreateElement("td")
			td := element.(styledText)
			cells[pos] = cell
			tds[pos] = td
			pos++
			setElementFromCell(td, cell)
			row.AppendChild(element)
		}
		table.AppendChild(row)
	}
	terminal.AppendChild(table)

	return &JSTerminal{
		width:   width,
		cells:   cells,
		tds:     tds,
		windows: make([]*window, 0, 16)}
}

// SetEventHandler implements the event handler registration of the terminal.
func (t *JSTerminal) SetEventHandler(eventHandler TEventHandler) {
	win := dom.GetWindow()
	win.AddEventListener("keydown", true, func(event dom.Event) {
		ev := event.(*dom.KeyboardEvent)
		if ev.CtrlKey() {
			return
		}
		event.PreventDefault()
		var tevent *TEvent
		if ev.CharCode() == 0 && ev.KeyCode() == 219 {
			tevent = &TEvent{Key: TQuest, Shift: ev.ShiftKey()}
		} else {
			tevent = &TEvent{Key: ev.KeyCode(), Shift: ev.ShiftKey()}
		}
		eventHandler.HandleEvent(tevent)
	})
}

// SetTimeout implements the defered function execution of the terminal.
func (t *JSTerminal) SetTimeout(fn func(), delay int) {
	win := dom.GetWindow()
	win.SetTimeout(fn, delay)
}

// Size reports the size of the terminal.
func (t *JSTerminal) Size() (int, int) {
	return t.width, len(t.cells) / t.width
}

// Width reports the width of the terminal.
func (t *JSTerminal) Width() int {
	return t.width
}

// Height reports the height of the terminal.
func (t *JSTerminal) Height() int {
	return len(t.cells) / t.width
}

// AddWindow adds a hidden window with a given name.
func (t *JSTerminal) AddWindow(name string, win TWindow) {
	x, y, width, height := win.Resize(t.Width(), t.Height())
	t.windows = append(t.windows, &window{
		name:   name,
		x:      x,
		y:      y,
		width:  width,
		height: height,
		window: win})
}

func (t *JSTerminal) updateWindow(win *window) {
	for i := 0; i < win.height; i++ {
		pos := (win.y+i)*t.width + win.x
		for j := 0; j < win.width; j++ {
			cell := win.window.CellAt(j, i)
			setElementFromDiff(t.tds[pos], cell, &t.cells[pos])
			pos++
		}
	}
}

// UpdateWindow shows a window on the terminal.
func (t *JSTerminal) UpdateWindow(name string) {
	for i, win := range t.windows {
		if win.name == name {
			win.visible = true
			copy(t.windows[1:], t.windows[0:i])
			t.windows[0] = win
			t.updateWindow(win)
			return
		}
	}
}

// Flush does nothing in this implemention because the DOM
// backend does not need this.
func (t *JSTerminal) Flush() {
	// Ignore mw.
}

// Quit does not shutdown the terminal because you do not
// want the terminal to be removed from the web page.
func (t *JSTerminal) Quit() bool {
	// Ignored.
	return false
}
