// Copyright 2014, 2015 by Sascha L. Teichmann
// Use of this source code is governed by the GPLv3 or later license
// that can be found in the LICENSE file.

package main

//  Terminal colors.
const (
	TBlack = int16(iota)
	TBlue
	TGreen
	TCyan
	TRed
	TMagenta
	TBrown
	TLightGray
	TGray
	TLightBlue
	TLightGreen
	TLightCyan
	TLightRed
	TLightMagenta
	TYellow
	TWhite
)

// Style attributes.
const (
	TNormal = int16(iota)
	TBold
)

// Cursor key codes.
const (
	TKeyCursorLeft = 37 + iota
	TKeyCursorUp
	TKeyCursorRight
	TKeyCursorDown
)

// Alphabet key codes.
const (
	TKeyA = 65 + iota
	TKeyB
	TKeyC
	TKeyD
	TKeyE
	TKeyF
	TKeyG
	TKeyH
	TKeyI
	TKeyJ
	TKeyK
	TKeyL
	TKeyM
	TKeyN
	TKeyO
	TKeyP
	TKeyQ
	TKeyR
	TKeyS
	TKeyT
	TKeyU
	TKeyV
	TKeyW
	TKeyX
	TKeyY
	TKeyZ
)

// Some special key codes.
const (
	TEsc   = 27
	TSpc   = 32
	TExcl  = 33
	TQuest = 63
)

// Number key codes.
const (
	TKey0 = 48 + iota
	TKey1
	TKey2
	TKey3
	TKey4
	TKey5
	TKey6
	TKey7
	TKey8
	TKey9
)

// Empty terminal cell.
var TBlankCell = MakeCell(' ', TRed, TBlack, TNormal)

// Cell combines a char rune and its graphical terminal attributes.
type Cell struct {
	Attr int16
	Ch   rune
}

// TWindow is the notion of window to be handled by the terminal.
type TWindow interface {
	CellAt(x, y int) Cell
	Resize(screenWidth, screenHeight int) (x, y, width, height int)
}

// TEvent is an event sent to the TEventHandler by the terminal.
type TEvent struct {
	Key   int
	Shift bool
}

// TEventHandler is invoked by the terminal if a event is arrived.
type TEventHandler interface {
	HandleEvent(event *TEvent)
}

// MakeAttr is a convenience function to fuse foreground color, background color
// and style attribute to a terminal attribute.
func MakeAttr(fg, bg, mode int16) int16 {
	return mode<<8 | bg<<4 | fg
}

// MakeCell is a convenience function to fuse a char and the graphical terminal
// attributes to a Cell.
func MakeCell(ch rune, fg, bg, mode int16) Cell {
	return Cell{Ch: ch, Attr: MakeAttr(fg, bg, mode)}
}

// CellAt returns the cell itself for every given x/y coordinate. Therefore
// it can be used as TWindow
func (cell Cell) CellAt(x, y int) Cell {
	return cell
}

// Resize as part of the TWindow interface is used to implement a fullscreen
// filling mechanism.
func (cell Cell) Resize(screenWidth, screenHeight int) (int, int, int, int) {
	return 0, 0, screenWidth, screenHeight
}

// Terminal encapsulates the functions of a terminal.
type Terminal interface {

	// Quit is to be called from the client to shutdown the terminal.
	// It reports if the operation succeed.
	Quit() bool

	// UpdateWindow brings a named window to front.
	UpdateWindow(name string)

	// AddWindow adds a hidden window with name to the terminal.
	// To show it call UpdateWindow
	AddWindow(name string, window TWindow)

	// Width reports the current width of the terminal.
	Width() int

	// Height reports the current height of the terminal.
	Height() int

	// Size reports the current width and height of the terminal.
	Size() (int, int)

	// SetTimeout sets a function to be executed in delay milliseconds.
	// The function is executed in the event loop of the terminal
	// so it does not interfere the event delivery and is therefore
	// race free.
	SetTimeout(fn func(), delay int)

	// SetEventHandler sets the mandatory event handler for the terminal.
	SetEventHandler(eventHandler TEventHandler)

	// Flush forces to bring all cached content to the terminal backend.
	Flush()
}
