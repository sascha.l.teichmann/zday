// Copyright 2014, 2015 by Sascha L. Teichmann
// Use of this source code is governed by the GPLv3 or later license
// that can be found in the LICENSE file.

// +build !js

package main

import (
	"flag"
	"log"
)

func main() {
	var col256 bool
	var terminal *TermboxTerminal
	var err error

	flag.BoolVar(&col256, "256", false, "Use 256 color mode.")
	flag.BoolVar(&col256, "2", false, "Use 256 color mode (shorthand).")

	flag.Parse()

	if terminal, err = NewTermboxTerminal(col256); err != nil {
		log.Fatal(err)
	}
	defer terminal.Close()

	game := NewGame(terminal)
	game.Start()
	if err := terminal.MainLoop(); err != nil {
		log.Fatal(err)
	}
}
