// Copyright 2014, 2015 by Sascha L. Teichmann
// Use of this source code is governed by the GPLv3 or later license
// that can be found in the LICENSE file.

// +build !js

package main

import (
	"errors"
	"time"

	"github.com/nsf/termbox-go"
)

var colorMapStd = [...]termbox.Attribute{
	TBlack:        termbox.ColorBlack,
	TBlue:         termbox.ColorBlue,
	TGreen:        termbox.ColorGreen,
	TCyan:         termbox.ColorCyan,
	TRed:          termbox.ColorRed,
	TMagenta:      termbox.ColorMagenta,
	TBrown:        termbox.ColorYellow | termbox.AttrBold,
	TLightGray:    termbox.ColorWhite | termbox.AttrBold,
	TGray:         termbox.ColorWhite,
	TLightBlue:    termbox.ColorBlue | termbox.AttrBold,
	TLightGreen:   termbox.ColorGreen | termbox.AttrBold,
	TLightCyan:    termbox.ColorCyan | termbox.AttrBold,
	TLightRed:     termbox.ColorRed | termbox.AttrBold,
	TLightMagenta: termbox.ColorMagenta | termbox.AttrBold,
	TYellow:       termbox.ColorYellow,
	TWhite:        termbox.ColorWhite,
}

var colorMap256 = [...]termbox.Attribute{
	TBlack:        termbox.ColorBlack,
	TBlue:         termbox.ColorBlue,
	TGreen:        termbox.ColorGreen,
	TCyan:         termbox.ColorCyan,
	TRed:          termbox.ColorRed,
	TMagenta:      termbox.ColorMagenta,
	TBrown:        termbox.ColorYellow | termbox.AttrBold,
	TLightGray:    termbox.Attribute(0xf5),
	TGray:         termbox.Attribute(0xef),
	TLightBlue:    termbox.ColorBlue | termbox.AttrBold,
	TLightGreen:   termbox.ColorGreen | termbox.AttrBold,
	TLightCyan:    termbox.ColorCyan | termbox.AttrBold,
	TLightRed:     termbox.ColorRed | termbox.AttrBold,
	TLightMagenta: termbox.ColorMagenta | termbox.AttrBold,
	TYellow:       termbox.ColorYellow,
	TWhite:        termbox.ColorWhite,
}

type window struct {
	name          string
	x, y          int
	width, height int
	visible       bool
	window        TWindow
}

type colorMap *[16]termbox.Attribute

// TermboxTerminal is a terminal implementation using Termbox.
type TermboxTerminal struct {
	quit         bool
	windows      []*window
	eventHandler TEventHandler
	timerFuncs   chan func()
	cmap         colorMap
}

// NewTermboxTerminal creates a new terminal with a flag if it should
// try to switch to 256 color mode. I this mode switching does not
// succeed the normal color mode is used.
func NewTermboxTerminal(color256 bool) (*TermboxTerminal, error) {
	if err := termbox.Init(); err != nil {
		return nil, err
	}
	termbox.HideCursor()
	cmap := &colorMapStd
	if color256 && termbox.SetOutputMode(termbox.Output256) == termbox.Output256 {
		termbox.Clear(termbox.ColorDefault, termbox.ColorDefault)
		if err := termbox.Sync(); err != nil {
			termbox.Close()
			return nil, err
		}
		cmap = &colorMap256
	}
	return &TermboxTerminal{
		quit:       false,
		windows:    make([]*window, 0, 16),
		timerFuncs: make(chan func()),
		cmap:       cmap}, nil
}

// Close shuts down terminal.
func (terminal *TermboxTerminal) Close() {
	close(terminal.timerFuncs)
	termbox.Close()
}

// MainLoop starts the event dispatcher of the terminal.
// An EventHandler has to be registered before.
func (terminal *TermboxTerminal) MainLoop() error {
	if terminal.eventHandler == nil {
		return errors.New("No event handler set in main loop.")
	}
	events := make(chan termbox.Event)
	defer close(events)
	go func() {
		defer func() { recover() }()
		for {
			events <- termbox.PollEvent()
		}
	}()

	for !terminal.quit {
		select {
		case event := <-events:
			switch event.Type {
			case termbox.EventKey:
				//log.Printf("%d '%c' %d\n", event.Ch, event.Ch, event.Key)
				if out, ok := makeKeyEvent(&event); ok {
					terminal.eventHandler.HandleEvent(&out)
				}
			case termbox.EventResize:
				terminal.handleResize(event.Width, event.Height)
			case termbox.EventError:
				return event.Err
			}
		case fn := <-terminal.timerFuncs:
			fn()
		}
	}

	return nil
}

func makeKeyEvent(in *termbox.Event) (TEvent, bool) {
	switch {
	case in.Ch >= 'a' && in.Ch <= 'z':
		return TEvent{Key: TKeyA + int(in.Ch-'a')}, true
	case in.Ch >= 'A' && in.Ch <= 'Z':
		return TEvent{Key: TKeyA + int(in.Ch-'A'), Shift: true}, true
	case in.Ch >= '0' && in.Ch <= '9':
		return TEvent{Key: TKey0 + int(in.Ch-'0')}, true
	case in.Ch == '?':
		return TEvent{Key: TQuest}, true
	case in.Ch == 0:
		switch in.Key {
		case termbox.KeyEsc:
			return TEvent{Key: TEsc}, true
		case termbox.KeySpace:
			return TEvent{Key: TSpc}, true
		case termbox.KeyArrowUp:
			return TEvent{Key: TKeyCursorUp}, true
		case termbox.KeyArrowDown:
			return TEvent{Key: TKeyCursorDown}, true
		case termbox.KeyArrowRight:
			return TEvent{Key: TKeyCursorRight}, true
		case termbox.KeyArrowLeft:
			return TEvent{Key: TKeyCursorLeft}, true
		}
	}
	return TEvent{}, false
}

func (terminal *TermboxTerminal) handleResize(width, height int) {
	n := len(terminal.windows)
	if n == 0 {
		return
	}
	termbox.Sync() // XXX: This is needed because of a bug in Termbox.
	// Resize the windows.
	for _, win := range terminal.windows {
		win.x, win.y, win.width, win.height = win.window.Resize(width, height)
	}
	p := 0
	for ; p < n; p++ {
		// Last visible full screen covers all other before.
		if win := terminal.windows[p]; win.visible &&
			win.x == 0 && win.y == 0 && win.width >= width && win.height >= height {
			break
		}
	}
	for p = min(p, n-1); p >= 0; p-- {
		if win := terminal.windows[p]; win.visible {
			terminal.update(win)
		}
	}
	termbox.Flush()
}

// Quit causes the MainLoop to return.
// Must be called from the EventHandler.
func (terminal *TermboxTerminal) Quit() bool {
	terminal.quit = true
	return true
}

func toStyle(a int16) termbox.Attribute {
	if a == TBold {
		return termbox.AttrBold
	}
	return 0
}

func toAttribute(cmap colorMap, attr int16) (fg, bg termbox.Attribute) {
	fg = cmap[attr&0xf] | toStyle((attr>>8)&3)
	bg = cmap[(attr>>4)&0xf]
	return
}

func (terminal *TermboxTerminal) update(win *window) {
	w, _ := termbox.Size()
	screen := termbox.CellBuffer()
	size := len(screen)
	cmap := terminal.cmap
	for i := 0; i < win.height; i++ {
		p := (i+win.y)*w + win.x
		if p < 0 {
			continue
		}
		if p >= size {
			break
		}
		for j := 0; j < win.width; j++ {
			if m := p + j; m >= 0 && m < size {
				c := win.window.CellAt(j, i)
				s := &screen[m]
				s.Ch = c.Ch
				s.Fg, s.Bg = toAttribute(cmap, c.Attr)
			}
		}
	}
}

// UpdateWindow shows a named window.
func (terminal *TermboxTerminal) UpdateWindow(name string) {
	for i, win := range terminal.windows {
		if win.name == name {
			win.visible = true
			copy(terminal.windows[1:], terminal.windows[0:i])
			terminal.windows[0] = win
			terminal.update(win)
			return
		}
	}
}

// Flush flushes the cached state of Termbox backend.
func (terminal *TermboxTerminal) Flush() {
	termbox.Flush()
}

// AddWindow adds a hidden window with a given name.
func (terminal *TermboxTerminal) AddWindow(name string, win TWindow) {
	x, y, width, height := win.Resize(terminal.Size())
	terminal.windows = append(terminal.windows, &window{
		name:   name,
		x:      x,
		y:      y,
		width:  width,
		height: height,
		window: win})
}

// Width reports the width of the terminal.
func (terminal *TermboxTerminal) Width() (w int) {
	w, _ = termbox.Size()
	return
}

// Height reports the height of the terminal.
func (terminal *TermboxTerminal) Height() (h int) {
	_, h = termbox.Size()
	return
}

// Size reports the size of the terminal.
func (terminal *TermboxTerminal) Size() (int, int) {
	return termbox.Size()
}

// SetTimeout implements the defered function execution of the terminal.
func (terminal *TermboxTerminal) SetTimeout(fn func(), delay int) {
	go func() {
		time.Sleep(time.Millisecond * time.Duration(delay))
		terminal.timerFuncs <- fn
	}()
}

// SetEventHandler implements the event handler registration of the terminal.
func (terminal *TermboxTerminal) SetEventHandler(eventHandler TEventHandler) {
	terminal.eventHandler = eventHandler
}
