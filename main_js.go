// Copyright 2014, 2015 by Sascha L. Teichmann
// Use of this source code is governed by the GPLv3 or later license
// that can be found in the LICENSE file.

// +build js wasm

package main

const (
	termWidth  = 80
	termHeight = 25
)

func main() {
	terminal := NewJSTerminal("zday-terminal", termWidth, termHeight)
	game := NewGame(terminal)
	game.Start()
	select {}
}
