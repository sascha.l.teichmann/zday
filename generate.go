// Copyright 2014, 2015 by Sascha L. Teichmann
// Use of this source code is governed by the GPLv3 or later license
// that can be found in the LICENSE file.

package main

func (m *worldmap) final() {
	for i := 0; i < 10; i++ {
		x1 := randRange(1, m.width-2)
		y1 := randRange(1, m.height-2)
		x2 := min(randRange(x1+1, m.width-1), x1+10)
		y2 := min(randRange(y1+1, m.height-1), y1+10)
		m.addRect(x1+1, y1+1, x2-1, y2-1, &tiles[tileFloor])
		m.addRect(x1, y1, x2, y2, &tiles[tileWall])
		m.addRect(x1-1, y1-1, x2+1, y2+1, &tiles[tileFloor])
		for j, n := 0, randRange(4, 8); j < n; j++ {
			m.putDoor(x1, y1, x2, y2, &tiles[tileFloor])
		}
	}

	m.addRect(0, 0, m.width-1, m.height-1, &tiles[tileWall])
	for j, n := 0, randRange(8, 15); j < n; j++ {
		m.putDoor(0, 0, m.width-1, m.height-1, &tiles[tileExit])
	}
}

func (m *worldmap) generate() {
	for i := 0; i < 130; i++ {
		x1 := randRange(1, m.width-2)
		y1 := randRange(1, m.height-2)
		x2 := min(randRange(x1+1, m.width-1), x1+10)
		y2 := min(randRange(y1+1, m.height-1), y1+10)
		m.addRect(x1+1, y1+1, x2-1, y2-1, &tiles[tileFloor])
		m.addRect(x1, y1, x2, y2, &tiles[tileWall])
		m.addRect(x1-1, y1-1, x2+1, y2+1, &tiles[tileFloor])
		for j, n := 0, randRange(2, 4); j < n; j++ {
			m.putDoor(x1, y1, x2, y2, &tiles[tileFloor])
		}
		for j, n := 0, randRange(0, 4); j < n; j++ {
			m.putDoor(x1, y1, x2, y2, &tiles[tileMachinery])
		}
	}

	for i := 0; i < 20; i++ {
		x1 := randRange(1, m.width-2)
		y1 := randRange(1, m.height-2)
		x2 := min(randRange(x1+1, m.width-1), x1+5)
		y2 := min(randRange(y1+1, m.height-1), y1+5)
		m.fillRect(x1, y1, x2, y2, &tiles[tileFloor])
	}

	m.addRect(0, 0, m.width-1, m.height-1, &tiles[tileWall])
}

func (m *worldmap) decorate() {
	for i, n := 0, randRange(5, 20); i < n; i++ {
		x := randRange(1, m.width-2)
		y := randRange(1, m.height-2)
		m.addTile(x, y, &tiles[tileCorpse])
	}
	for i, n := 0, randRange(25, 40); i < n; i++ {
		x := randRange(1, m.width-2)
		y := randRange(1, m.height-2)
		m.addTile(x, y, &tiles[tileCrate])
	}
	for i := 0; i < 5; i++ {
		for {
			x := randRange(1, m.width-2)
			y := randRange(1, m.height-2)
			if f := m.getField(x, y); f.t == &tiles[tileFloor] {
				f.t = &tiles[tileEscalatorDown]
				break
			}
		}
	}
}

func (m *worldmap) fillRect(x1, y1, x2, y2 int, t *tile) {
	for y := y1; y <= y2; y++ {
		for x := x1; x <= x2; x++ {
			m.addTile(x, y, t)
		}
	}
}

func (m *worldmap) addRect(x1, y1, x2, y2 int, t *tile) {
	for x := x1; x <= x2; x++ {
		m.addTile(x, y1, t)
		m.addTile(x, y2, t)
	}
	for y := y1 + 1; y < y2; y++ {
		m.addTile(x1, y, t)
		m.addTile(x2, y, t)
	}
}

func (m *worldmap) addTile(x, y int, t *tile) {
	if m.inside(x, y) {
		pos := y*m.width + x
		m.fields[pos].t = t
	}
}

func (m *worldmap) putDoor(x1, y1, x2, y2 int, t *tile) {
	var x, y int
	if randBool() {
		if x2-x1 > 2 {
			x = randRange(x1+1, x2-1)
			if randBool() {
				y = y1
			} else {
				y = y2
			}
			m.addTile(x, y, t)
		}
	} else {
		if y2-y1 > 2 {
			y = randRange(y1+1, y2-1)
			if randBool() {
				x = x1
			} else {
				x = x2
			}
			m.addTile(x, y, t)
		}
	}
}
