// Copyright 2014, 2015 by Sascha L. Teichmann
// Use of this source code is governed by the GPLv3 or later license
// that can be found in the LICENSE file.

package main

import "strconv"

const (
	msgNothingUsefulHere = "There's nothing useful here."
	msgSearchingCrate    = "Gently opening the crate..."
	msgSearchingCorpse   = "Searching the corpse..."
	msgFoundNothing      = "Found nothing."
	msgFoundSomething    = "Found something."
	msgNoMoreRoom        = "No more room."
	msgGoneAlready       = "Gone already."
	msgUselesCrap        = "Useless crap!"
	msgItsNotThere       = "It's not there."
	msgUseless           = "Useless."
	msgEmpty             = "Empty."
	msgSplat             = "Splat!"
	msgBroken            = "Broken."
	msgOuch              = "Ouch!"
	msgBurns             = "Burns!"
	msgChomp             = "Chomp!"
	msgNobodyHere        = "Nobody here."
	msgClick             = "Click!"
	msgSneakyBastard     = "Sneaky bastard!"
	msgSpak              = "Spak!"
	msgChangedYourMind   = "Changed your mind, eh?"
	msgNoMoreTargets     = "No more targets."
	msgHere              = "Here."
	msgCareful           = "Careful! Those gears can shred you into pieces."
	msgBonk              = "Bonk!"
	msgPardonMe          = "Pardon me."
	msgYawn              = "Yawn..."
	msgWhereToRun        = "Where to run?"
	msgPantPant          = "Pant, pant..."
)

var splashText = []string{
	"@@@@@@@@             @@@@@@@    @@@@@@   @@@ @@@",
	"@@@@@@@@             @@@@@@@@  @@@@@@@@  @@@ @@@",
	"     @@!             @@!  @@@  @@!  @@@  @@! !@@",
	"    !@!              !@!  @!@  !@!  @!@  !@! @!!",
	"   @!!    @!@!@!@!@  @!@  !@!  @!@!@!@!   !@!@!",
	"  !!!     !!!@!@!!!  !@!  !!!  !!!@!!!!    @!!!",
	" !!:                 !!:  !!!  !!:  !!!    !!:",
	":!:                  :!:  !:!  :!:  !:!    :!:",
	" :: ::::              :::: ::  ::   :::     ::",
	": :: : :             :: :  :    :   : :     :",
	"",
	"",
	"        --== Press <Space> to start ==--"}

var storyText = []string{
	"On your trip to  the mall  you  suddenly heard  a loud noise",
	"and you lost your consciousness. When you woke up, you found",
	"the  whole store  pitch-black,  totally ruined  and  all the",
	"other people either dead or zombified.",
	"",
	"You  have to leave,  but there are  several  storyes to pass",
	"through, and the zombies look hungry.",
	"",
	"Fortunately there  are all  those goodies from the store and",
	"the guns of the guards are lying around.",
	"",
	"With a bit of luck  you should make it  to the ground floor.",
	"",
	"           Press <Space> to start your escape..."}

var quitText = []string{"Do you really want to quit? (y/n)"}

var winText = []string{
	"__     __                    _       _",
	"\\ \\   / /                   (_)     | |",
	" \\ \\_/ /__  _   _  __      ___ _ __ | |",
	"  \\   / _ \\| | | | \\ \\ /\\ / / | '_ \\| |",
	"   | | (_) | |_| |  \\ V  V /| | | | |_|",
	"   |_|\\___/ \\__,_|   \\_/\\_/ |_|_| |_(_)",
	"",
	"",
	"       Finally made it to safety!",
	"",
	"    Do you want to play again? (y/n)"}

var loseText = []string{
	"     @@@@@@@@   @@@@@@   @@@@@@@@@@   @@@@@@@@",
	"    @@@@@@@@@  @@@@@@@@  @@@@@@@@@@@  @@@@@@@@",
	"    !@@        @@!  @@@  @@! @@! @@!  @@!",
	"    !@!        !@!  @!@  !@! !@! !@!  !@!",
	"    !@! @!@!@  @!@!@!@!  @!! !!@ @!@  @!!!:!",
	"    !!! !!@!!  !!!@!!!!  !@!   ! !@!  !!!!!:",
	"    :!!   !!:  !!:  !!!  !!:     !!:  !!:",
	"     ::: ::::  ::   :::  :::     ::    :: ::::",
	"     :: :: :    :   : :   :      :    : :: ::",
	"",
	"Aaargh! They got me! Plant an orchid on my grave...",
	"        Do you want to play again? (y/n)",
	"",
	"     @@@@@@   @@@  @@@  @@@@@@@@  @@@@@@@   @@@",
	"    @@@@@@@@  @@@  @@@  @@@@@@@@  @@@@@@@@  @@@",
	"    @@!  @@@  @@!  @@@  @@!       @@!  @@@  @@!",
	"    !@!  @!@  !@!  @!@  !@!       !@!  @!@  !@",
	"    @!@  !@!  @!@  !@!  @!!!:!    @!@!!@!   @!@",
	"    !@!  !!!  !@!  !!!  !!!!!:    !!@!@!    !!!",
	"    !!:  !!!  :!:  !!:  !!:       !!: :!!",
	"    ::::: ::    ::::     :: ::::  ::   :::   ::",
	"     : :  :      :      : :: ::    :   : :  :::"}

var helpText = [][]string{{
	"Keyboard controls:",
	"",
	"Use cursor keys to move around.",
	"Use 'v' to dash (vault) two fields into a direction.",
	"Use 'z' to shoot (zap) at highlighted enemy.",
	"Use 'Z' to aim at a different enemy.",
	"Use 'x' to eXamine crates and corpses.",
	"Use 'X' to use the escalators.",
	"Use keys 'a' to 'u' to use items from inventory.",
	"Use keys 'A' to 'U' to discard items from inventory.",
	"Use Space to wait a round.",
	"Use Esc to quit.",
	"Use '?' to show this help.",
	"",
	"Press <Space> for more...",
}, {
	"Legend:",
	"",
	"@ the player",
	"Z the zombies",
	". floor",
	"# wall",
	": trash and rubble",
	"+ various body parts",
	"% corpses",
	"& crates and boxes",
	"$ dangerous machinery",
	"< escalators",
	"X emergency exit",
	"",
	"Press <Space> to resume game...",
}}

func helpWindows() func() string {
	count := 0
	return func() string {
		if count >= len(helpText) {
			return ""
		}
		window := "help" + strconv.Itoa(count)
		count++
		return window
	}
}

func registerTextWindows(t Terminal) {
	t.AddWindow("splash", NewTextWindow(splashText, MakeAttr(TRed, TBlack, TNormal)))
	t.AddWindow("quit", NewTextWindow(quitText, MakeAttr(TYellow, TBlack, TBold)))
	t.AddWindow("story", NewTextWindow(storyText, MakeAttr(TRed, TBlack, TNormal)))
	t.AddWindow("win", NewTextWindow(winText, MakeAttr(TWhite, TBlack, TBold)))
	t.AddWindow("lose", NewTextWindow(loseText, MakeAttr(TMagenta, TBlack, TNormal)))

	for i, help := range helpText {
		t.AddWindow("help"+strconv.Itoa(i),
			NewTextWindow(help, MakeAttr(TWhite, TBlack, TNormal)))
	}
}
